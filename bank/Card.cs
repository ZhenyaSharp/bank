﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bank
{
    class Card
    {
        private ulong cardNumber;
        private int balance;

        public Card(ulong cardNumber, int balance)
        {
            this.cardNumber = cardNumber;
            this.balance = balance;
        }

        public ulong Number
        {
            get { return cardNumber; }
        }

        public int Balance
        {
            get
            {
                return balance;
            }
            set { balance=value; }
        }

    }
}
