﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bank
{
    class Client
    {
        private string fullName;

        private List<Card> cards;

        public Client(string fullName)
        {
            this.fullName = fullName;
            cards = new List<Card>();
        }

        public List<Card> Cards
        {
            get { return cards; }
        }

        public string FullName
        {
            get { return fullName; }
        }

        public void AddCard(Card card)
        {
            cards.Add(card);
        }

        private void PrintInfoAboutCards()
        {
            for (int i = 0; i < cards.Count; i++)
            {
                Console.WriteLine($"Карта{i+1}  {cards[i].Number} Баланс ({cards[i].Balance})");
            }
        }

        public void PrintPersonalInfo()
        {
            Console.WriteLine($"ФИО клиента:{fullName}");
            PrintInfoAboutCards();
            Console.WriteLine("----------");
        }
    }
}
