﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace bank
{
    class BankManager
    {
        private List<Client> clients;

        public BankManager()
        {
            clients = new List<Client>();
        }

        public void Interface()
        {
            bool isEnd;

            do
            {
                Console.Clear();
                isEnd = false;

                Console.WriteLine("Добро пожаловать в наш Банк. Выберите пункт меню: ");

                int menuPoint = ConsoleHelper.InputIntInRange("1)Добавить нового клиента\n2)Добавить карту клиенту\n3)Перевод средств\n4)Посмотреть информацию о клиентах\n0)Выход\n", 0, 4);

                switch (menuPoint)
                {
                    case 1:
                        AddClient();

                        Console.WriteLine($"Ура {clients[clients.Count - 1].FullName}! Теперь вы клиент нашего Банка. Cделаем Вам карту:");

                        AddCardForClient(clients[clients.Count - 1]);

                        break;

                    case 2:
                        if (!CheckForEmptiness())
                        {
                            PrintNamesClients();

                            int numClient = ConsoleHelper.InputIntInRange("Выберите кому добавить карту: ", 1, clients.Count);

                            AddCardForClient(clients[numClient - 1]);
                        }

                        break;

                    case 3:
                        if (!CheckForEmptiness())
                        {
                            int sumTransfer= Debit();

                            if (sumTransfer == 0)
                            {
                                break;
                            }

                            MoneyTransfer(sumTransfer);
                        }
                        break;


                    case 4:
                        if (!CheckForEmptiness())
                        {
                            PrintInfoAboutClients();
                            ConsoleHelper.WaitingEnter();
                        }
                        break;

                    case 0:
                        isEnd = true;
                        break;
                }

            } while (!isEnd);
        }

        private int Debit() // списание
        {
            Console.Clear();
            PrintNamesClients();

            int sender = ConsoleHelper.InputIntInRange("Выберите кто переводит деньги:", 1, clients.Count);
            Console.Clear();

            clients[sender - 1].PrintPersonalInfo();

            int cardSender = ConsoleHelper.InputIntInRange($"Выберите c какой карты {clients[sender - 1].FullName} переводит деньги:", 1, clients[sender - 1].Cards.Count);

            if (clients[sender - 1].Cards[cardSender - 1].Balance ==0)
            {
                Console.WriteLine("Недостаточно средств на счёте");
                ConsoleHelper.WaitingEnter();
                return 0;
            }
            else
            {
                int sumTransfer = ConsoleHelper.InputIntInRange("Введите сумму перевода", 1,
                    clients[sender - 1].Cards[cardSender - 1].Balance);

                clients[sender - 1].Cards[cardSender - 1].Balance -= sumTransfer;
                return sumTransfer;
            }
        }

        private void MoneyTransfer(int sumTransfer) // зачисление
        {
            Console.Clear();
            PrintNamesClients();

            int receiver = ConsoleHelper.InputIntInRange("Выберите кто получает деньги:", 1, clients.Count);
            Console.Clear();

            clients[receiver - 1].PrintPersonalInfo();

            int cardReceiver = ConsoleHelper.InputIntInRange($"Выберите на какую карту {clients[receiver - 1].FullName} получит деньги:", 1, clients[receiver - 1].Cards.Count);

            clients[receiver - 1].Cards[cardReceiver - 1].Balance += sumTransfer;

            Console.WriteLine();
            Console.WriteLine($"Платеж на сумму {sumTransfer} успешно проведён");
            ConsoleHelper.WaitingEnter();
        }

        private void AddClient()
        {
            Console.WriteLine("Введите ФИО клиента: ");
            string newFullName = Console.ReadLine();

            Client client = new Client(newFullName);

            clients.Add(client);
        }

        private void AddCardForClient(Client client)
        {
            ulong newNumber = ConsoleHelper.InputUlong("Введите номер карты:");

            int newBalance = ConsoleHelper.InputInt("Введите баланс: ");

            Card card = new Card(newNumber, newBalance);

            client.AddCard(card);
        }

        private void PrintNamesClients()
        {
            for (int i = 0; i < clients.Count; i++)
            {
                Console.WriteLine($"({i + 1}) " + clients[i].FullName);
            }
        }

        private void PrintInfoAboutClients()
        {
            foreach (var client in clients)
            {
                client.PrintPersonalInfo();
            }
        }

        private bool CheckForEmptiness() // проверка на пустоту
        {
            if (clients.Count == 0)
            {
                Console.WriteLine("У банка нет клиентов");
                ConsoleHelper.WaitingEnter();
                return true;
            }
            return false;
        }
    }
}
